#  Copyright 2020-     Sipke Vriend
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

"""
An example of execution the robot framework via its run entry point, and providing an instance of a listener.
The listener is closely tied to a Dialogs library showing how one can override the built in Dialogs library with
your own and indirectly tie it to the listener.
Example execution:
python ProgrammaticListenerExample.py TestRobotDialogs.robot
"""

import os, sys
from robot import run
from RobotTestListenerDialogLibrary import DialogsListener


def run_example(test_path):
    """
    Kick off the robot test run
    :param test_path: name of a robot file to execute
    :return: None
    """
    #path = os.path.dirname(os.path.abspath(__file__))
    #sys.path.insert(0, path)
    run(test_path, listener=DialogsListener(), variable="CUSTOMDIALOGS:ListenerDialogs")


if __name__ == '__main__':
    import sys

    if len(sys.argv) < 2:
        print("Please supply robot script name as argument")

    run_example(sys.argv[1])
