# On the command line execute
#   robot TestRobotDialogs.robot
# OR from and IDE with virtual environment setup
#   python.exe venv/Lib/site-packages/robot/run.py TestRobotDialogs.robot

*** Settings ***
Documentation    A test to check how robot framework dialogs work
Library          Dialogs

*** Test Cases ***
Test title
    [Tags]    DEBUG
    Pause Execution            Please select ok
    ${user} =     Get Selection From User    Select user  one  two  three
    Log    ${user}
    ${users} =    Get Selections From User   Select multiple users  one  two  three
    Log    ${users}
    ${value} =    Get Value From User        Please enter a value
    Execute Manual Step        Please select pass
    Execute Manual Step        Please select fail

*** Keywords ***

