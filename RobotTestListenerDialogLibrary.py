#  Copyright 2008-2015 Nokia Networks
#  Copyright 2016-     Robot Framework Foundation
#  Copyright 2020-     Sipke Vriend
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

"""A test library providing dialogs for interacting with users.

Modified directly from the robot framework Dialogs library with the
addition os a v3 Listener, which is provided to this Dialog library
via a suite variable. Simply as an example of how one can provide a full
listener experience.

``Dialogs`` is Robot Framework's standard library that provides means
for pausing the test execution and getting input from users. The
dialogs are slightly different depending on whether tests are run on
Python, IronPython or Jython but they provide the same functionality.

Long lines in the provided messages are wrapped automatically. If you want
to wrap lines manually, you can add newlines using the ``\\n`` character
sequence.

The library has a known limitation that it cannot be used with timeouts
on Python. Support for IronPython was added in Robot Framework 2.9.2.
"""

from robot.libraries.BuiltIn import BuiltIn


class DialogsListener:
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    ROBOT_LISTENER_API_VERSION = 3

    def __init__(self):
        self.ROBOT_LIBRARY_LISTENER = self
        self._hello = None
        print("\nDialogsListener.__init__")

    def start_suite(self, name, result):
        print("\nDialogsListener.start_suite", name, result.starttime)
        BuiltIn().set_suite_variable("${my listener}", self)

    def start_test(self, name, result):
        print("\nDialogsListener.start_test", name, result.status, result.passed)

    def end_test(self, name, result):
        print("\nDialogsListener.end_test", name, result.status, result.passed)

    def end_suite(self, name, result):
        print("\nDialogsListener.end_suite", name, result.endtime)
        if self._hello:
            print('Someone says', '"' + self._hello + '"')
        else:
            print("No one said anything")

    def close(self):
        print("\nDialogsListener.close()")

    def hello(self, message="hi"):
        self._hello = message