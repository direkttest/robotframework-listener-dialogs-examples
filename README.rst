Robot Listener And Dialogs Examples
--------------------------------------------

The `Robot Framework <http://robotframework.org/>`_ provides a `listener api <https://robotframework.org/robotframework/latest/RobotFrameworkUserGuide.html#listener-interface>`_ which allows the test system to follow test suite progress.
Additionally it provides a standard `Dialogs library <https://robotframework.org/robotframework/latest/libraries/Dialogs.html>`_, which allows for test steps to request information from the user.

This repository provides a few very simple examples of using both Dialogs and the v2 and v3 of the listener api.

You will need to `install the pre-requisites <https://robotframework.org/robotframework/latest/RobotFrameworkUserGuide.html#installation-instructions>`_ (into your system or preferably a virtual environment in your preferred
IDE).

The three examples are

------------------------------------
Testing the standard Dialogs library
------------------------------------
On the command line execute
   ``robot TestRobotDialogs.robot``
OR from and IDE with virtual environment setup
   ``python.exe venv/Lib/site-packages/robot/run.py TestRobotDialogs.robot``

The next sections shows the Test step followed by the dialog displayed, as executed on a Windows 10 machine.

.. |br| raw:: html

  <br/>

:Test Step:  ``Pause Execution            Please select ok``

.. image:: snippets/TestRobotDialogs/PleaseSelectOk.PNG
   :width: 300
   :alt: Please Select OK Dialog

|br|

:Test Step:  ``${user} =     Get Selection From User    Select user  one  two  three``

.. image:: snippets/TestRobotDialogs/GetSelectionFromUser.PNG
   :width: 300
   :alt: Get Selection From User

|br|

:Test Step:  ``${users} =    Get Selections From User   Select multiple users  one  two  three``

.. image:: snippets/TestRobotDialogs/GetSelectionsFromUser.PNG
   :width: 300
   :alt: Get Selections From User

|br|

:Test Step:  ``${value} =    Get Value From User        Please enter a value``

.. image:: snippets/TestRobotDialogs/GetValueFromUser.PNG
   :width: 300
   :alt: Get Value From User

|br|

:Test Step:  ``Execute Manual Step        Please select pass``

.. image:: snippets/TestRobotDialogs/PleaseSelectPass.PNG
   :width: 300
   :alt: Execute Manual Step (Pass)

|br|

:Test Step:  ``Execute Manual Step        Please select fail``

.. image:: snippets/TestRobotDialogs/PleaseSelectFail.PNG
   :width: 300
   :alt: Execute Manual Step (Fail)

|br|

As part of the fail case, the user is asked for error details

.. image:: snippets/TestRobotDialogs/ErrorMessageEntryBox.PNG
   :width: 300
   :alt: Enter the reason for the failure

-----------------------------
Testing a Robot Listener
-----------------------------

The example provides two very simple robot listeners one for v2 of the api and the other v3.

To execute on command line
  ``robot --listener RobotTestListener2.py TestRobotListener.robot``
Or in an IDE with virtual environment
   ``python.exe venv/Lib/site-packages/robot/run.py --listener RobotTestListener2.py TestRobotListener.robot``

Change Listener to RobotTestListener3.py to compare to v3 listener api. There is small differences in the parameters
received.

If you look at the python files the listener simply prints some test items, so we can see that the methods are being
called.

The output for v3 case is::

    RobotTestListener3.__init__
    ==============================================================================
    TestRobotListener :: A test to check how robot listener works
    ==============================================================================
    start_suite TestRobotListener 20200624 14:02:20.947
    Test Case One
    start_test Test Case One FAIL False
    end_test Test Case One PASS True
    | PASS |
    ------------------------------------------------------------------------------
    Test Case Two
    start_test Test Case Two FAIL False
    end_test Test Case Two PASS True
    | PASS |
    ------------------------------------------------------------------------------
    Test Case Three
    start_test Test Case Three FAIL False
    end_test Test Case Three FAIL False
    | FAIL |
    "Force" != "A Fail"
    ------------------------------------------------------------------------------
    end_suite TestRobotListener 20200624 14:02:21.165
    TestRobotListener :: A test to check how robot listener works         | FAIL |
    3 critical tests, 2 passed, 1 failed
    3 tests total, 2 passed, 1 failed

As can be seen the listener printouts include start_suite, start_test, end_test and end_suite, with the first two tests
passing and the last failing due to a forced failure with the string check ``"Force" != "A Fail"``

---------------------------------
Programmatic Listener and Dialog
---------------------------------

Both those cases can be useful, but the listener and dialog are instantiated as needed, so there is no integration
between the two.

The third test is a case of a programmatically instantiated listener, which is then passed to a custom Dialog as a
`suite variable <https://robotframework.org/robotframework/2.1.2/libraries/BuiltIn.html#Set%20Suite%20Variable>`_.

To execute run
   ``python.exe ProgrammaticListenerExample.py TestRobotCustomDialogs.robot``

The test is programmatically executed because we use the robot 'run' entry and pass in a custom dialog variable::

    run(test_path, listener=DialogsListener(), variable="CUSTOMDIALOGS:ListenerDialogs")

The CUSTOMDIALOGS variable is then used in the robot file to load our custom dialog library::

    Library          ${CUSTOMDIALOGS}

The output of the execution is::

    DialogsListener.__init__
    ==============================================================================
    TestRobotCustomDialogs :: A test to check how robot framework dialogs work
    ==============================================================================

    DialogsListener.start_suite TestRobotCustomDialogs 20200624 14:03:36.945
    Test title
    DialogsListener.start_test Test title FAIL False

    DialogsListener.end_test Test title FAIL False
    | FAIL |
    Something wrong
    ------------------------------------------------------------------------------

    DialogsListener.end_suite TestRobotCustomDialogs 20200624 14:04:18.130
    Someone says "This example skips the pausing of execution"
    TestRobotCustomDialogs :: A test to check how robot framework dial... | FAIL |
    1 critical test, 0 passed, 1 failed
    1 test total, 0 passed, 1 failed
    ==============================================================================

You will note that the tests steps in this robot file are identical to that of the first test, however because we have
a custom dialog library, the Pause Execution step (which previously yielded a popup message) is completely
skipped, instead the custom dialog module

writes a message to the listener::

    listener = BuiltIn().get_variable_value("${my listener}")
    listener.hello(message)

which the listener (RobotTestListenerDialogLibrary.py) then prints out at the end in end_suite::

    Someone says "This example skips the pausing of execution"

The suite variable is available to the custom dialog because the listener added itself as the ${my listener} varialbe
when the suite_start method was called::

    def start_suite(self, name, result):
        print("\nDialogsListener.start_suite", name, result.starttime)
        BuiltIn().set_suite_variable("${my listener}", self)

.. note:: In the TestRobotCustomDialogs.robot file, we provide two Dialogs libraries, the default one and our custom one. This is simply because the custom one only 'overrides' one of the keywords/functions.
