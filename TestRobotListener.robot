#
# To execute on command line
#   robot --listener RobotTestListener2.py TestRobotListener.robot
# Or in an IDE with virtual environment
#   python.exe venv/Lib/site-packages/robot/run.py --listener RobotTestListener2.py TestRobotListener.robot
# Change Listener to RobotTestListener3.py to compare to v3 listener api.
#

*** Settings ***
Documentation    A test to check how robot listener works
Library          Dialogs

*** Test Cases ***
Test Case One
    [Tags]    Tag1
    Log    "Test Case One"

Test Case Two
    Log    "Test Case Two"

Test Case Three
    Log    "Test Case Three"
    Should Be Equal    "Force"  "A Fail"


*** Keywords ***
