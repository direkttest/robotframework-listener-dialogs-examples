#  Copyright 2020-     Sipke Vriend
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.


class RobotTestListener3:
    ROBOT_LISTENER_API_VERSION = 3

    def __init__(self):
        print("RobotTestListener3.__init__")
        pass

    def start_suite(self, name, result):
        print("start_suite", name, result.starttime)

    def start_test(self, name, result):
        print("\nstart_test", name, result.status, result.passed)

    def end_test(self, name, result):
        print("end_test", name, result.status, result.passed)

    def end_suite(self, name, result):
        print("end_suite", name, result.endtime)

    def close(self):
        print("RobotTestListener3.close()")