#  Copyright 2020-     Sipke Vriend
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.


class RobotTestListener2:
    ROBOT_LISTENER_API_VERSION = 2

    def __init__(self):
        print("RobotTestListener2.__init__")
        pass

    def start_suite(self, name, attrs):
        print("start_suite", "%s '%s'\n" % (name, attrs['doc']))

    def start_test(self, name, attrs):
        tags = ' '.join(attrs['tags'])
        print("\nstart_test", "- %s '%s' [ %s ] :: " % (name, attrs['doc'], tags))

    def end_test(self, name, attrs):
        if attrs['status'] == 'PASS':
            print("end_test", 'PASS\n')
        else:
            print("end_test", 'FAIL: %s\n' % attrs['message'])

    def end_suite(self, name, attrs):
        print("end_suite", '%s\n%s\n' % (attrs['status'], attrs['message']))

    def close(self):
        print("RobotTestListener2.close()")