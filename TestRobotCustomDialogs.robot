#
# To execute run
#   python.exe ProgrammaticListenerExample.py TestRobotCustomDialogs.robot
# Note there is a close tie between listener and the custom dialog
#

*** Settings ***
Documentation    A test to check how to integrate a robot listener and dialogs
# The caller must ensure CUSTOMDIALOGS variable is set. e.g. CUSTOMDIALOGS:ListenerDialogs
Library          ${CUSTOMDIALOGS}
# We only need to provide the default Dialogs library if our CUSTOMDIALOGS library does not provide all keywords
Library          Dialogs

*** Test Cases ***
Test title
    [Tags]    DEBUG
    Set Library Search Order    ${CUSTOMDIALOGS}  Dialogs
    Pause Execution            This example skips the pausing of execution
    ${user} =     Get Selection From User    Select user  one  two  three
    Log    ${user}
    ${users} =    Get Selections From User   Select multiple users  one  two  three
    Log    ${users}
    ${value} =    Get Value From User        Please enter a value
    Execute Manual Step        Please select pass
    Execute Manual Step        Please select fail

*** Keywords ***

