#  Copyright 2008-2015 Nokia Networks
#  Copyright 2016-     Robot Framework Foundation
#  Copyright 2020-     Sipke Vriend
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

"""A test library providing dialogs for interacting with users.

Modified directly from the robot framework Dialogs library with the
addition os a v3 Listener, which is provided to this Dialog library
via a suite variable. Simply as an example of how one can provide a full
listener experience.

``Dialogs`` is Robot Framework's standard library that provides means
for pausing the test execution and getting input from users. The
dialogs are slightly different depending on whether tests are run on
Python, IronPython or Jython but they provide the same functionality.

Long lines in the provided messages are wrapped automatically. If you want
to wrap lines manually, you can add newlines using the ``\\n`` character
sequence.

The library has a known limitation that it cannot be used with timeouts
on Python. Support for IronPython was added in Robot Framework 2.9.2.
"""

from robot.libraries.BuiltIn import BuiltIn
from robot.version import get_version


__version__ = get_version()
__all__ = ['execute_manual_step', 'get_value_from_user',
           'get_selection_from_user', 'pause_execution', 'get_selections_from_user']


def pause_execution(message='Test execution paused. Press OK to continue.'):
    """Provide an alternate Pause execution which doe not actually pause, but
    simply gets the listener variable and writes a hello message to the listener.
    The listener will print this message in its end_suite function.
    There is no functional purpose here, rather, just an example of how to
    connect a listener to a library.

    ``message`` is the message that will be given to the listener.
    """
    listener = BuiltIn().get_variable_value("${my listener}")
    listener.hello(message)
